/**
 * @file game_manager.c
 * @author NoiKioN (nnightknight@gmail.com)
 * @brief Game manager is tasked to handle the drawing of the elements to the screen
 * @version 0.1
 * @date 2018-11-05
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#include "entity_manager.h"

/**
 * @brief Create a Game Manager object
 * 
 * @param max_elements 
 * @return GameManager* 
 */
EntityManager *CreateEntityManager() {
	/* Init game manager */
	EntityManager *gameManager = (EntityManager *) malloc(sizeof(EntityManager));
	/* Initialize game manager */
	gameManager->first = NULL;

	ENTITY_MANAGER = gameManager;
	/* Returning the game manager pointer */
	return gameManager;
}

/**
 * @brief Enqueue the next game element to the game queue
 *
 * @param gameManager
 * @param element
 */
void Enqueue(Entity *entity) {
	EntityListElement *newEntityListElement = (EntityListElement *) malloc(sizeof(EntityListElement));
	newEntityListElement->entity = entity;
	newEntityListElement->nextElement = NULL;

	if (ENTITY_MANAGER->first == NULL) {
		newEntityListElement->previousElement = NULL;
		ENTITY_MANAGER->first = newEntityListElement;
		ENTITY_MANAGER->last = ENTITY_MANAGER->first;
		return;
	}
	else{
		ENTITY_MANAGER->last->nextElement = newEntityListElement;
		newEntityListElement->previousElement = ENTITY_MANAGER->last;
		ENTITY_MANAGER->last = newEntityListElement;
	}
}

/**
 * @brief Dequeue the completed element
 * 
 * @param gameManager
 */
void Dequeue(Entity *entity) {
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		if (iterator->entity == entity){
			entity->Destroy(entity);

			if (iterator->previousElement != NULL) {
				iterator->previousElement->nextElement = iterator->nextElement;
			}
			if (iterator->nextElement != NULL) {
				iterator->nextElement->previousElement = iterator->previousElement;
			}
			free(iterator);
			return;
		}
	}
}

/**
 * @brief Free the allocated memory of the game manager
 * 
 * @param gameManager
 */
void DestroyGameManager() {
	/* Free list elements and then the game manager itself */
	EntityListElement *temp = ENTITY_MANAGER->first;
	while (temp != NULL) {
		EntityListElement *last_element = temp->nextElement;
		if (temp->entity->Destroy != NULL) {
			temp->entity->Destroy(temp->entity);
		}
		DestroyEntity(temp->entity);
		free(temp);
		temp = last_element;
	}
	free(ENTITY_MANAGER);
}

Entity *GetEntityByName(char *name){
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		if (strcmp(iterator->entity->name, name) == 0){
			return iterator->entity;
		}
	}
	return NULL;
}

void CallbackWithEntities(void Function(Entity *entity)){
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		Function(iterator->entity);
	}
}

void CallbackWithEntitiesWithOtherEntities(void Function(Entity *firstEntity, Entity *secondEntity)){
	for (EntityListElement *firstIter = ENTITY_MANAGER->first; firstIter != NULL; firstIter = firstIter->nextElement) {
		for (EntityListElement *secondIter = ENTITY_MANAGER->first; secondIter != NULL; secondIter = secondIter->nextElement) {
			if (firstIter != secondIter){
				Function(firstIter->entity, secondIter->entity);
			}
		}
	}
}

void InitEntities(){
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		if (iterator->entity->data == NULL && iterator->entity->Init != NULL) {
			iterator->entity->Init(iterator->entity);
		}
	}
}

void UpdateEntities(){
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		if (iterator->entity->Update != NULL) {
			iterator->entity->Update(iterator->entity);
		}
	}
}

void LateUpdateEntities(){
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		if (iterator->entity->LateUpdate != NULL) {
			iterator->entity->LateUpdate(iterator->entity);
		}
	}
}

void PrepareToDrawEntities(){
	for (EntityListElement *iterator = ENTITY_MANAGER->first; iterator != NULL; iterator = iterator->nextElement) {
		if (iterator->entity->PrepareToDraw != NULL) {
			iterator->entity->PrepareToDraw(iterator->entity);
		}
	}
}