/**
 * @file sprite.c
 * @author NoiKioN (nnightknight@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2018-11-05
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#include "sprite.h"

static void insertClipRectListElement(ClipRectListElement **first, ClipRectListElement **last, ClipRectListElement *toBeInserted){
	if(*first != NULL) {
		(*last)->nextClipRectListElement = toBeInserted;
		*last = toBeInserted;
	}
	else {
		*first = toBeInserted;
		*last = toBeInserted;
	}
}

static ClipRectListElement *CreateClipRects(SingleSprite *sprite, SDL_Rect *clipRect) {
	ClipRectListElement *first = NULL;
	ClipRectListElement *last = NULL;
	for (int i = 0; i < sprite->numberOfFrames; ++i) {

		ClipRectListElement *clipRectListElement = (ClipRectListElement *)malloc(sizeof(ClipRectListElement));

		clipRectListElement->clipRect = clipRect[i];
		clipRectListElement->nextClipRectListElement = NULL;
		insertClipRectListElement(&first, &last, clipRectListElement);
	}

	return first;
}

static void DestroyClipRects(SingleSprite *sprite){
	ClipRectListElement *temp = sprite->firstClipRectListElement;
	while(temp != NULL){
		ClipRectListElement *next = temp->nextClipRectListElement;
		free(temp);
		temp = next;
	}
}

SingleSprite *CreateSingleSprite(int width, int height, SDL_Texture *spriteTexture, int frameTime, int numberOfFrames, SDL_Rect *clips, SDL_Rect collider) {
	SingleSprite *singleSprite = (SingleSprite *) malloc(sizeof(SingleSprite));

	singleSprite->width = width;
	singleSprite->height = height;
	singleSprite->spriteTexture = spriteTexture;
	singleSprite->currentFrameTime = 0;
	singleSprite->frameTime = frameTime;
	singleSprite->numberOfFrames = numberOfFrames;
	singleSprite->collider = collider;

	singleSprite->firstClipRectListElement = CreateClipRects(singleSprite, clips);
	singleSprite->currentClipRectListElement = singleSprite->firstClipRectListElement;

	return singleSprite;
}

void DestroySingleSprite(SingleSprite *sprite){
	DestroyClipRects(sprite);
	free(sprite);
}

static void stepClipRect(SingleSprite *sprite, double drawAcceleration){
	if (SDL_GetTicks() > sprite->currentFrameTime + (sprite->frameTime * drawAcceleration)){
		if (sprite->currentClipRectListElement->nextClipRectListElement != NULL) {
			sprite->currentClipRectListElement = sprite->currentClipRectListElement->nextClipRectListElement;
			sprite->currentFrameTime = SDL_GetTicks();
		}
		else{
			sprite->currentClipRectListElement = sprite->firstClipRectListElement;
			sprite->currentFrameTime = SDL_GetTicks();
		}
	}
}

void PrepareToDrawSingleSprite(SingleSprite *singleSprite, double drawAcceleration) {
	if (singleSprite->numberOfFrames > 1) {
		stepClipRect(singleSprite, drawAcceleration);
	}
}