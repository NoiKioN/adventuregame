//
// Created by nnigh on 11/21/2018.
//

#include "physics_manager.h"

PhysicsManager *CreatePhysicsManager(){
	PhysicsManager *physicsManager = (PhysicsManager *)malloc(sizeof(PhysicsManager));

	physicsManager->gravityVelocity = 10.0;

	PHYSICS_MANAGER = physicsManager;

	return physicsManager;
}

void CollisionCheckEntity(Entity *entity, Entity *collidedEntity){
	if (!entity->movingEntity.isStatic && !SDL_RectEmpty(&entity->movingEntity.collider) && entity->CollisionCallback != NULL) {
		SDL_Rect movedCheckedCollider = GetMovingEntityCollider(entity->movingEntity, ConvertVectorDoubleToVectorInt(entity->precisePosition));
		SDL_Rect movedOtherCollider = GetMovingEntityCollider(collidedEntity->movingEntity, ConvertVectorDoubleToVectorInt(collidedEntity->precisePosition));
		if (SDL_HasIntersection(&movedCheckedCollider, &movedOtherCollider)){
			SDL_Rect collisionRect;
			SDL_IntersectRect(&movedCheckedCollider, &movedOtherCollider, &collisionRect);
			if (entity->movingEntity.velocityVector.y < entity->movingEntity.velocityVector.x && collisionRect.h < collisionRect.w && collisionRect.y > ((int)entity->precisePosition.y) + entity->movingEntity.collider.h){
				entity->CollisionCallback(entity, collidedEntity, COLLISION_ABOVE);
				entity->precisePosition.y -= collisionRect.h;
				entity->movingEntity.velocityVector.y = 0;
			}
			else if (collisionRect.h < collisionRect.w && collisionRect.y < ((int)entity->precisePosition.y) + entity->movingEntity.collider.h) {
				entity->CollisionCallback(entity, collidedEntity, COLLISION_BELOW);
				entity->precisePosition.y += collisionRect.h;
				entity->movingEntity.velocityVector.y = 0;
			}
			else if (collisionRect.w < collisionRect.h && collisionRect.x > ((int)entity->precisePosition.x) + entity->movingEntity.collider.w){
				entity->CollisionCallback(entity, collidedEntity, COLLISION_RIGHT);
				entity->precisePosition.x -= collisionRect.w - 1;
				entity->movingEntity.velocityVector.x = 0;
			}
			else if (collisionRect.w < collisionRect.h && collisionRect.x < ((int)entity->precisePosition.x) + entity->movingEntity.collider.w) {
				entity->CollisionCallback(entity, collidedEntity, COLLISION_LEFT);
				entity->precisePosition.x += collisionRect.w - 1;
				entity->movingEntity.velocityVector.x = 0;
			}
		}
	}
	if ((int)entity->precisePosition.x + entity->movingEntity.collider.x < 0){
		entity->precisePosition.x = 0 - entity->movingEntity.collider.x;
	}
	if ((int)entity->precisePosition.x + entity->movingEntity.collider.x + entity->movingEntity.collider.w > MAP_WIDTH){
		entity->precisePosition.x = MAP_WIDTH - entity->movingEntity.collider.x - entity->movingEntity.collider.w;
	}
	if ((int)entity->precisePosition.y + entity->movingEntity.collider.y < 0){
		entity->precisePosition.y = 0 - entity->movingEntity.collider.y;
	}
	if ((int)entity->precisePosition.y + entity->movingEntity.collider.y + entity->movingEntity.collider.h > MAP_HEIGHT){
		entity->precisePosition.y = MAP_HEIGHT - entity->movingEntity.collider.y - entity->movingEntity.collider.h;
	}
}

void UpdateTimeElapsed(double timeInMS){
	PHYSICS_MANAGER->deltaTime = timeInMS;
}

void PhysicsUpdateEntity(Entity *entity){
	MovingEntity *movingEntity = &entity->movingEntity;
	if (!movingEntity->isStatic){
		entity->precisePosition.x += entity->movingEntity.velocityVector.x * PHYSICS_MANAGER->deltaTime * entity->scale;
		entity->movingEntity.velocityVector.y -= PHYSICS_MANAGER->gravityVelocity * PHYSICS_MANAGER->deltaTime * 50;
		if (entity->movingEntity.velocityVector.y < -100){
			entity->movingEntity.velocityVector.y = -100;
		}
		entity->precisePosition.y -= entity->movingEntity.velocityVector.y * PHYSICS_MANAGER->deltaTime;
	}
}

void DestroyPhysicsManager(){
	free(PHYSICS_MANAGER);
}