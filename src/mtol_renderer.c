//
// Created by NoiKioN on 11/17/2018.
//

#include "mtol_renderer.h"

static void DrawSprite(SingleSprite *sprite, int scale, VectorInt posVector){
	SDL_Rect renderQuad = {posVector.x, posVector.y, sprite->width * scale, sprite->height * scale};
	SDL_RenderCopy(MTOL_RENDERER, sprite->spriteTexture, &sprite->currentClipRectListElement->clipRect, &renderQuad);
}

static void DrawSpriteMirrored(SingleSprite *sprite, int scale, VectorInt posVector) {
	SDL_Rect renderQuad = {posVector.x, posVector.y, sprite->width * scale, sprite->height * scale};
	SDL_RenderCopyEx(MTOL_RENDERER, sprite->spriteTexture, &sprite->currentClipRectListElement->clipRect, &renderQuad,
					 0, &(SDL_Point) {.x = sprite->width / 2, .y = sprite->height / 2}, SDL_FLIP_HORIZONTAL);
}

void CreateMTOLRenderer(SDL_Renderer *renderer){
	MTOL_RENDERER = renderer;
}

void Draw(Entity *entity){
	Entity *cameraEntity = GetEntityByName("camera");

	VectorInt posVector = ConvertVectorDoubleToVectorInt(entity->precisePosition);
	RepositionToCameraView(cameraEntity, &posVector);

	if (entity->currentSprite != NULL) {
		if (entity->currentDirection == 1) {
			DrawSprite(entity->currentSprite, entity->scale, posVector);
		}else if (entity->currentDirection == -1){
			DrawSpriteMirrored(entity->currentSprite, entity->scale, posVector);
		}
	}
}

void RenderClear(){
	SDL_RenderClear(MTOL_RENDERER);
}

void RenderPresent(){
	SDL_RenderPresent(MTOL_RENDERER);
}