//
// Created by NoiKioN on 11/18/2018.
//

#include "game_loader.h"

static char *ConcatPath(char *path, char *fileName){
	char *temp = (char *)malloc(sizeof(char) * (strlen(path) + 1 + strlen(fileName) + 1));
	strcpy(temp, path);
	strcat(temp, "/");
	strcat(temp, fileName);
	strcat(temp, "\0");

	return temp;
}

static enum SpriteDefTypes GetSpriteDefType(char *str){
	for (int i = 0; i <= SPRITE_SIZE; ++i) {
		if (strstr(str, spriteDefTypeNames[i])){
			return i;
		}
	}
	return INT16_MIN;
}

static void *ExtractSpriteDefs(MTOL_Definitions *definitions){
	SpriteDefs *newSpriteDefs = (SpriteDefs *)malloc(sizeof(SpriteDefs));
	int currentClipNumber = 0;
	int clipNumbers[(definitions->numberOfDefs)];
	char *clipValues[(definitions->numberOfDefs)];
	for (int i = 0; i < definitions->numberOfDefs; ++i) {
		switch (GetSpriteDefType(definitions->keys[i])){
			case SPRITE_TEXTURE:
				newSpriteDefs->name = ConcatPath("", definitions->values[i]);
				newSpriteDefs->texturePath = ConcatPath("../assets/sprites", definitions->values[i]);
				break;
			case SPRITE_FRAMES: {
				char *ptr;
				newSpriteDefs->numberOfFrames = strtol(definitions->values[i], &ptr, 10);
				break;
			}
			case SPRITE_FRAME_TIME: {
				char *ptr;
				newSpriteDefs->frameTime = strtol(definitions->values[i], &ptr, 10);
				break;
			}
			case SPRITE_CLIP:{
				char *ptr;
				if (isdigit(*(definitions->keys[i] + strlen(spriteDefTypeNames[SPRITE_CLIP])))) {
					clipNumbers[currentClipNumber - 1] = strtol(
							definitions->keys[i] + strlen(spriteDefTypeNames[SPRITE_CLIP]), &ptr, 10);
				}
				clipValues[currentClipNumber] = definitions->values[i];
				++currentClipNumber;
				break;
			}
			case SPRITE_COLLIDER: {
				char *ptr;
				newSpriteDefs->collider.x = strtol(definitions->values[i], &ptr, 10);
				++ptr;
				newSpriteDefs->collider.y = strtol(ptr, &ptr, 10);
				++ptr;
				newSpriteDefs->collider.w = strtol(ptr, &ptr, 10);
				++ptr;
				newSpriteDefs->collider.h = strtol(ptr, &ptr, 10);
				break;
			}
			case SPRITE_SIZE: {
				char *ptr;
				newSpriteDefs->width = strtol(definitions->values[i], &ptr, 10);
				++ptr;
				newSpriteDefs->height = strtol(ptr, &ptr, 10);
				break;
			}
			default:
				break;
		}
	}
	SDL_Rect *clips = (SDL_Rect *)malloc(sizeof(SDL_Rect) * currentClipNumber);

	if (currentClipNumber > 1) {
		for (int i = 0; i < currentClipNumber; ++i) {
			for (int j = 0; j < currentClipNumber; ++j) {
				if (clipNumbers[i] < clipNumbers[j]) {
					int tempI = clipNumbers[i];
					clipNumbers[i] = clipNumbers[j];
					clipNumbers[j] = tempI;
					char *tempC = clipValues[i];
					clipValues[i] = clipValues[j];
					clipValues[j] = tempC;
				}
			}
		}

		for (int i = 0; i < currentClipNumber; ++i) {
			char *ptr;
			clips[i].x = strtol(clipValues[i], &ptr, 10);
			++ptr;
			clips[i].y = strtol(ptr, &ptr, 10);
			clips[i].w = newSpriteDefs->width;
			clips[i].h = newSpriteDefs->height;
		}
	}
	else{
		char *ptr;
		clips->x = strtol(clipValues[0], &ptr, 10);
		++ptr;
		clips->y = strtol(ptr, &ptr, 10);
		clips->w = newSpriteDefs->width;
		clips->h = newSpriteDefs->height;
		newSpriteDefs->frameTime = 0;
		newSpriteDefs->numberOfFrames = 1;
	}

	newSpriteDefs->clips = clips;
	return newSpriteDefs;
}

static enum MapDefTypes GetMapDefType(char *str){
	for (int i = 0; i <= COLLIDER; ++i) {
		if (strstr(str, mapDefTypeNames[i])){
			return i;
		}
	}
	return INT16_MIN;
}

static void *ExtractMapDefinitions(MTOL_Definitions *definitions){
	MapDefs *mapDefs = (MapDefs *)malloc(sizeof(MapDefs));
	int tileNumbers[definitions->numberOfDefs];
	char *tileNames[definitions->numberOfDefs];
	int tileCount = 0;
	int layerNumbers[definitions->numberOfDefs];
	char *mapLayer[definitions->numberOfDefs];
	int layerCount = 0;
	char *colliders = NULL;
	for (int i = 0; i < definitions->numberOfDefs; ++i) {
		switch(GetMapDefType(definitions->keys[i])){
			case MAP_SIZE: {
				char *ptr;
				mapDefs->mapSize.x = strtol(definitions->values[i], &ptr, 10);
				++ptr;
				mapDefs->mapSize.y = strtol(ptr, &ptr, 10);
				break;
			}
			case TILE_SIZE: {
				char *ptr;
				mapDefs->tileSize.x = strtol(definitions->values[i], &ptr, 10);
				++ptr;
				mapDefs->tileSize.y = strtol(ptr, &ptr, 10);
				break;
			}
			case MAP_TILE: {
				char *ptr = definitions->keys[i] + strlen(mapDefTypeNames[MAP_TILE]);
				tileNumbers[tileCount] = strtol(ptr, &ptr, 10);
				tileNames[tileCount] = (char *)malloc(sizeof(char) * (strlen(definitions->values[i]) + 1));
				strcpy(tileNames[tileCount], definitions->values[i]);
				++tileCount;
				break;
			}
			case MAP_LAYER: {
				char *ptr = definitions->keys[i] + strlen(mapDefTypeNames[MAP_LAYER]);
				layerNumbers[layerCount] = strtol(ptr, &ptr, 10);
				mapLayer[layerCount] = (char *)malloc(sizeof(char) * (strlen(definitions->values[i]) + 1));
				strcpy(mapLayer[layerCount], definitions->values[i]);
				++layerCount;
				break;
			}
			case COLLIDER: {
				colliders = (char *)malloc(sizeof(char) * (strlen(definitions->values[i]) + 1));
				strcpy(colliders, definitions->values[i]);
				break;
			}
			default:
				break;
		}
	}

	/************************ TILES ************************/

	for (int i = 0; i < tileCount; ++i) {
		for (int j = 0; j < tileCount; ++j) {
			if (tileNumbers[i] < tileNumbers[j]){
				int temp = tileNumbers[i];
				tileNumbers[i] = tileNumbers[j];
				tileNumbers[j] = temp;
				char *tempName = tileNames[i];
				tileNames[i] = tileNames[j];
				tileNames[j] = tempName;
			}
		}
	}

	++tileCount;
	mapDefs->mapTiles = (char **)malloc(sizeof(char *) * tileCount);
	mapDefs->mapTiles[0] = NULL;

	for (int i = 1; i < tileCount; ++i) {
		mapDefs->mapTiles[i] = tileNames[i - 1];
	}
	mapDefs->numOfTiles = tileCount;

	/************************ TILES END ************************/
	/************************ LAYERS ************************/

	for (int i = 0; i < layerCount; ++i) {
		for (int j = 0; j < layerCount; ++j) {
			if (layerNumbers[i] < layerNumbers[j]){
				int temp = layerNumbers[i];
				layerNumbers[i] = layerNumbers[j];
				layerNumbers[j] = temp;
				char *tempName = mapLayer[i];
				mapLayer[i] = mapLayer[j];
				mapLayer[j] = tempName;
			}
		}
	}

	int ***layers = (int ***)malloc(sizeof(int **) * layerCount);
	for (int i = 0; i < layerCount; ++i) {
		char *ptr = mapLayer[i];
		layers[i] = (int **)malloc(sizeof(int *) * mapDefs->mapSize.y);
		for (int y = 0; y < mapDefs->mapSize.y; ++y) {
			layers[i][y] = (int *)malloc(sizeof(int) * mapDefs->mapSize.x);
			for (int x = 0; x < mapDefs->mapSize.x; ++x) {
				layers[i][y][x] = strtol(ptr, &ptr, 10);
				++ptr;
			}
		}
	}

	for (int i = 0; i < layerCount; ++i) {
		free(mapLayer[i]);
	}

	mapDefs->numOfLayers = layerCount;
	mapDefs->layers = layers;

	/************************ LAYERS END ************************/
	/************************ COLLIDERS ************************/

	char *ptr = colliders;
	int colliderCount = 0;
	int test;
	while (ptr < colliders + strlen(colliders)){
		++colliderCount;
		for (int i = 0; i < 4; ++i) {
			strtol(ptr, &ptr, 10);
			++ptr;
		}
	}

	int x, y, w, h;
	SDL_Rect *colliderRects = (SDL_Rect *)malloc(sizeof(SDL_Rect) * colliderCount);
	ptr = colliders;
	for (int i = 0; i < colliderCount; ++i) {
		x = strtol(ptr, &ptr, 10);
		++ptr;
		y = strtol(ptr, &ptr, 10);
		++ptr;
		w = strtol(ptr, &ptr, 10);
		++ptr;
		h = strtol(ptr, &ptr, 10);
		colliderRects[i] = (SDL_Rect){.x = x * mapDefs->tileSize.x, .y = y * mapDefs->tileSize.y, .w = w * mapDefs->tileSize.x, .h = h * mapDefs->tileSize.x};
		++ptr;
	}
	free(colliders);
	mapDefs->numberOfColliders = colliderCount;
	mapDefs->colliders = colliderRects;

	/************************ COLLIDERS END ************************/

	return mapDefs;
}

static void AddDefs(DefinitionListElement *newDefinitionListElement){
	if (GAME_LOADER->first == NULL) {
		GAME_LOADER->first = newDefinitionListElement;
		GAME_LOADER->last = newDefinitionListElement;
	}
	else{
		GAME_LOADER->last->next = newDefinitionListElement;
		GAME_LOADER->last = GAME_LOADER->last->next;
	}
}

static void LoadDefs(char *path, enum DefinitionListElementType type,  void *(*ExtractDefs)(MTOL_Definitions *definitions)){
	DIR *spritesFolder = opendir(path);
	if (spritesFolder == NULL){
		SDL_Log("Cannot open folder %s", path);
	}

	struct dirent *file = readdir(spritesFolder);
	while(file != NULL) {
		if (strcmp(file->d_name, ".") != 0 && strcmp(file->d_name, "..") != 0) {
			char *newPath = ConcatPath(path, file->d_name);
			MTOL_Definitions *defs = GetDefinitions(newPath);
			free(newPath);

			DefinitionListElement *newDefinitionListElement = (DefinitionListElement *) malloc(sizeof(DefinitionListElement));
			file->d_name[(strlen(file->d_name) - strlen(".mtol"))] = '\0';
			newDefinitionListElement->name = (char *) malloc(sizeof(char) * (strlen(file->d_name) + 1));
			strcpy(newDefinitionListElement->name, file->d_name);
			newDefinitionListElement->next = NULL;
			newDefinitionListElement->defs = ExtractDefs(defs);
			newDefinitionListElement->type = type;
			AddDefs(newDefinitionListElement);

			FreeMTOLDefs(defs);
		}
		file = readdir(spritesFolder);
	}
}

GameLoader *CreateGameLoader(){
	 GameLoader *gameLoader = (GameLoader *)malloc(sizeof(GameLoader));
	 gameLoader->first = NULL;
	 gameLoader->last = NULL;

	 GAME_LOADER = gameLoader;

	 LoadDefs("../assets/sprites/sprite_defs", SPRITE, ExtractSpriteDefs);
	 LoadDefs("../assets/map", MAP, ExtractMapDefinitions);
}

void *GetDefs(char *name){
	for (DefinitionListElement *iterator = GAME_LOADER->first; iterator != NULL; iterator = iterator->next) {
		if (strcmp(iterator->name, name) == 0){
			return iterator->defs;
		}
	}
}

static void DestroyDefinition(enum DefinitionListElementType type, void *defs) {
	switch (type){
		case SPRITE: {
			SpriteDefs *spriteDefs = (SpriteDefs *) defs;
			free(spriteDefs->name);
			free(spriteDefs->texturePath);
			free(spriteDefs->clips);
			free(spriteDefs);
			break;
		}
		case MAP: {
			MapDefs *mapDefs = (MapDefs *) defs;
			for (int i = 0; i < mapDefs->numOfLayers; ++i) {
				for (int j = 0; j < mapDefs->mapSize.y; ++j) {
						free(mapDefs->layers[i][j]);
				}
				free(mapDefs->layers[i]);
			}
			free(mapDefs->layers);
			for (int i = 0; i < mapDefs->numOfTiles; ++i) {
				free(mapDefs->mapTiles[i]);
			}
			free(mapDefs->mapTiles);
			free(mapDefs->colliders);
			free(mapDefs);
			break;
		}
		default:
			break;
	}
}

void DestroyGameLoader(){
	DefinitionListElement *temp = GAME_LOADER->first;
	while(temp != NULL){
		DefinitionListElement *next = temp->next;
		free(temp->name);
		DestroyDefinition(temp->type, temp->defs);
		free(temp);
		temp = next;
	}
	free(GAME_LOADER);
}