//
// Created by NoiKioN on 11/8/2018.
//

#include "event_manager.h"

static void SetCurrentInputEvent(EventManager *eventManager, InputKeyEvent inputKeyEvent, InputPressType inputPressType){
	eventManager->currentInputEvent->inputKeyEvent = inputKeyEvent;
	eventManager->currentInputEvent->inputPressType = inputPressType;
}

static InputEvent *CreateInputEvent(InputKeyEvent inputKeyEvent, InputPressType inputPressType){
	InputEvent *inputEvent = (InputEvent *)malloc(sizeof(InputEvent));
	inputEvent->inputKeyEvent = inputKeyEvent;
	inputEvent->inputPressType = inputPressType;
	return inputEvent;
}

static bool InputsEqual(InputEvent *inputEvent1, InputEvent *inputEvent2){
	if (inputEvent1->inputPressType == inputEvent2->inputPressType && inputEvent1->inputKeyEvent == inputEvent2->inputKeyEvent){
		return true;
	}
	return false;
}

EventManager *CreateEventManager(){
	EventManager *eventManager = (EventManager *)malloc(sizeof(EventManager));
	eventManager->acceptInput = true;
	eventManager->first = NULL;
	eventManager->last = NULL;
	eventManager->currentInputEvent = CreateInputEvent(EMPTY_KEY_EVENT, EMPTY_PRESS_TYPE);

	EVENT_MANAGER = eventManager;
}

void RegisterToInput(Entity *entity, InputKeyEvent inputKeyEvent, InputPressType inputPressType, void (*Callback)(Entity *entity, InputEvent *inputEvent)){
	InputEvent *newInputEvent = (InputEvent *)malloc(sizeof(InputEvent));
	newInputEvent->inputKeyEvent = inputKeyEvent;
	newInputEvent->inputPressType = inputPressType;

	InputRegistryListElement *newInputRegistryListElement = (InputRegistryListElement *)malloc(sizeof(InputRegistryListElement));
	newInputRegistryListElement->inputListened = newInputEvent;
	newInputRegistryListElement->Callback = Callback;
	newInputRegistryListElement->entity = entity;
	newInputRegistryListElement->next = NULL;

	if (EVENT_MANAGER->first == NULL){
		EVENT_MANAGER->first = newInputRegistryListElement;
		EVENT_MANAGER->last = EVENT_MANAGER->first;
	}
	else{
		EVENT_MANAGER->last->next = newInputRegistryListElement;
		EVENT_MANAGER->last = EVENT_MANAGER->last->next;
	}
}

void HandleInputEvent(SDL_Event *event){
	InputPressType inputPressType = RELEASED;
	if (event->type == SDL_KEYDOWN){
		inputPressType = PRESSED;
	}
	InputEvent *inputEvent = EVENT_MANAGER->currentInputEvent;
	switch (event->key.keysym.sym){
		case W:
			SetCurrentInputEvent(EVENT_MANAGER, W, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case A:
			SetCurrentInputEvent(EVENT_MANAGER, A, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case S:
			SetCurrentInputEvent(EVENT_MANAGER, S, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case D:
			SetCurrentInputEvent(EVENT_MANAGER, D, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case UP:
			SetCurrentInputEvent(EVENT_MANAGER, UP, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case LEFT:
			SetCurrentInputEvent(EVENT_MANAGER, LEFT, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case DOWN:
			SetCurrentInputEvent(EVENT_MANAGER, DOWN, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case RIGHT:
			SetCurrentInputEvent(EVENT_MANAGER, RIGHT, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		case SPACE:
			SetCurrentInputEvent(EVENT_MANAGER, SPACE, inputPressType);
			CallbackToEntity(inputEvent);
			break;
		default:
			break;
	}
	SetCurrentInputEvent(EVENT_MANAGER, EMPTY_KEY_EVENT, EMPTY_PRESS_TYPE);
}

void CallbackToEntity(InputEvent *inputEvent){
	for (InputRegistryListElement *iterator = EVENT_MANAGER->first; iterator != NULL ; iterator = iterator->next) {
		if (InputsEqual(inputEvent, iterator->inputListened)){
			iterator->Callback(iterator->entity, inputEvent);
			break;
		}
	}
}

void DestroyEventManager(){
	InputRegistryListElement *temp = EVENT_MANAGER->first;
	while (temp != NULL) {
		InputRegistryListElement *last = temp->next;
		free(temp->inputListened);
		free(temp);
		temp = last;
	}
	free(EVENT_MANAGER->currentInputEvent);
	free(EVENT_MANAGER);
}