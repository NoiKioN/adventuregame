//
// Created by nnigh on 11/9/2018.
//

#include "sprite_manager.h"

static bool InitSDLIMG(){
	if (IMG_Init(IMG_INIT_PNG) == 0){
		SDL_Log("IMG Could not be initialized.");
		return false;
	}
	return true;
}

static SDL_Texture *LoadTexture(SDL_Renderer *renderer, char *texture_path){
	SDL_Surface *loadedSurface = IMG_Load(texture_path);
	if (loadedSurface == NULL){
		SDL_Log("Surface could not be loaded: %s", texture_path);
		return NULL;
	}
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
	SDL_FreeSurface(loadedSurface);

	return texture;
}

SpriteManager *CreateSpriteManager(SDL_Renderer *renderer){
	if (!InitSDLIMG()){
		return NULL;
	}
	SpriteManager *spriteManager = (SpriteManager *)malloc(sizeof(SpriteManager));
	if (spriteManager == NULL){
		SDL_Log("Could not allocate game loader.");
		return NULL;
	}
	spriteManager->renderer = renderer;

	spriteManager->first = NULL;
	spriteManager->last = NULL;

	SPRITE_MANAGER = spriteManager;

	return spriteManager;
}

SDL_Texture *GetSpriteTexture(char *name){
	for (SpriteTextureListElement *iterator = SPRITE_MANAGER->first; iterator != NULL; iterator = iterator->next) {
		if (strcmp(iterator->name, name) == 0) {
			return iterator->spriteTexture;
		}
	}
	return NULL;
}

SDL_Texture *AddSpriteTexture(char *name, char *texturePath){
	SDL_Texture *texture = LoadTexture(SPRITE_MANAGER->renderer, texturePath);
	SpriteTextureListElement *spriteTextureListElement = (SpriteTextureListElement *)malloc(sizeof(SpriteTextureListElement));
	spriteTextureListElement->name = (char *)malloc(sizeof(char) * (strlen(name) + 1));
	strcpy(spriteTextureListElement->name, name);
	spriteTextureListElement->spriteTexture = texture;
	spriteTextureListElement->next = NULL;

	if (SPRITE_MANAGER->first == NULL){
		SPRITE_MANAGER->first = spriteTextureListElement;
		SPRITE_MANAGER->last = spriteTextureListElement;
	}
	else {
		SPRITE_MANAGER->last->next = spriteTextureListElement;
		SPRITE_MANAGER->last = spriteTextureListElement;
	}

	return texture;
}

void RemoveSpriteTexture(char *name){
	SpriteTextureListElement *last = SPRITE_MANAGER->first;
	for (SpriteTextureListElement *iterator = SPRITE_MANAGER->first; iterator != NULL; iterator = iterator->next){
		if (iterator->name == name){
			SpriteTextureListElement *temp = iterator->next;
			free(iterator->name);
			SDL_DestroyTexture(iterator->spriteTexture);
			free(iterator);
			last->next = temp;
		}
		last = iterator;
	}
}

void DestroySpriteManager(){
	SpriteTextureListElement *temp = SPRITE_MANAGER->first;
	while(temp != NULL){
		SpriteTextureListElement *next = temp->next;
		free(temp->name);
		SDL_DestroyTexture(temp->spriteTexture);
		free(temp);
		temp = next;
	}
	free(SPRITE_MANAGER);
}