//
// Created by nnigh on 11/14/2018.
//

#include "vector.h"

VectorDouble CreateVectorDouble(double x, double y){
    return (VectorDouble){.x = x, .y = y};
}

void SetVectorDouble(VectorDouble *pVectorDouble, double x, double y){
    (*pVectorDouble).x = x;
    (*pVectorDouble).y = y;
}

VectorDouble ConvertVectorIntToVectorDouble(VectorInt vectorInt){
	return CreateVectorDouble((double)vectorInt.x, (double)vectorInt.y);
}

VectorInt GetVectorIntFromVectorDouble(VectorDouble vectorDouble){
	return (VectorInt){.x = (int)vectorDouble.x, .y = (int)vectorDouble.y};
}

void CopyVectorDouble(VectorDouble *pVectorDouble, VectorDouble vectorDouble){
    (*pVectorDouble).x = vectorDouble.x;
    (*pVectorDouble).y = vectorDouble.y;
}

void MultiplyVectorDouble(VectorDouble *pVectorDouble, VectorDouble vectorDouble){
    (*pVectorDouble).x *= vectorDouble.x;
    (*pVectorDouble).y *= vectorDouble.y;
}

VectorDouble MultiplyVectorDoubleWithC(VectorDouble vectorDouble, double c){
	return CreateVectorDouble(vectorDouble.x * c, vectorDouble.y * c);
}

void AddVectorDouble(VectorDouble *pVectorDouble, VectorDouble vectorDouble){
    (*pVectorDouble).x += vectorDouble.x;
    (*pVectorDouble).y += vectorDouble.y;
}

VectorInt CreateVectorInt(int x, int y){
	return (VectorInt){.x = x, .y = y};
}

void CopyVectorInt(VectorInt *pVectorInt, VectorInt vectorInt){
	(*pVectorInt).x = vectorInt.x;
	(*pVectorInt).y = vectorInt.y;
}

void SetVectorInt(VectorInt *vector, int x, int y){
    (*vector).x = x;
    (*vector).y = y;
}

void AddVectorInt(VectorInt *vector, int x, int y){
	vector->x += x;
	vector->y += y;
}

VectorInt ConvertVectorDoubleToVectorInt(VectorDouble vectorDouble){
	CreateVectorInt((int)vectorDouble.x, (int)vectorDouble.y);
}