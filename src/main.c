#include "game.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <stdbool.h>

int main(int argc, char *argv[]) {
    Game *game = StartGame("MTOLegends", 1280, 720, false);
    GameLoop(game);
    EndGame(game);
    SDL_Quit();
    return 0;
}
