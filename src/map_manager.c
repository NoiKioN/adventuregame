//
// Created by nnigh on 11/19/2018.
//

#include "map_manager.h"

MapManager *CreateMapManager(){
	MapManager *mapManager = (MapManager *)malloc(sizeof(MapManager));

	mapManager->cameraEntity = NULL;
	MAP_HEIGHT = 0;
	MAP_WIDTH = 0;

	MAP_MANAGER = mapManager;
}

void LoadMap(){
	MapDefs *mapDefs = GetDefs("map");

	SDL_Surface *mapSurface = SDL_CreateRGBSurface(0, mapDefs->mapSize.x * 32, mapDefs->mapSize.y * 32, 32, 0, 0, 0, 0);
	for (int i = 0; i < mapDefs->numOfLayers; ++i) {
		for (int y = 0; y < mapDefs->mapSize.y; ++y) {
			for (int x = 0; x < mapDefs->mapSize.x; ++x) {
				char *name = mapDefs->mapTiles[mapDefs->layers[i][y][x]];
				if (name != NULL) {
					SpriteDefs *spriteDefs = GetDefs(name);
					if (spriteDefs != NULL) {
						SDL_Surface *tempSurface = IMG_Load(spriteDefs->texturePath);
						if (tempSurface != NULL) {
							SDL_Rect tempRect = (SDL_Rect) {.x = x * spriteDefs->width, .y = y *
																							 spriteDefs->height, .w = spriteDefs->width, .h = spriteDefs->height};
							SDL_BlitSurface(tempSurface, spriteDefs->clips, mapSurface, &tempRect);
						}
						SDL_FreeSurface(tempSurface);
					}
				}
			}
		}
	}
	SDL_Texture *mapTexture = SDL_CreateTextureFromSurface(MTOL_RENDERER, mapSurface);
	RegisterMap("map", "map", 0, mapTexture, (SDL_Rect){.x = 0, .y = 0, .w = mapDefs->mapSize.x * 32, .h = mapDefs->mapSize.y * 32});
	for (int i = 0; i < mapDefs->numberOfColliders; ++i) {
		RegisterCollider(mapDefs->colliders[i]);
	}
	SDL_FreeSurface(mapSurface);
	RegisterPlayer();
	RegisterCamera();

	MAP_HEIGHT = mapDefs->mapSize.y * mapDefs->tileSize.x;
	MAP_WIDTH = mapDefs->mapSize.x * mapDefs->tileSize.y;

	MAP_MANAGER->cameraEntity = GetEntityByName("camera");
}

Entity *GetMainCamera(){
	return MAP_MANAGER->cameraEntity;
}

void DestroyMapManager(){
	free(MAP_MANAGER);
}

SDL_Rect GetCurrentColliderRect(){

}