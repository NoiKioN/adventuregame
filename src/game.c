/**
 * @file game.c
 * @author NoiKioN (nnightknight@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2018-11-05
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#include "test/debugmalloc.h"

#include "game.h"

static bool InitSDL(Game *game, SDL_Window **gwindow, SDL_Renderer **grenderer){
    /* Init SDL */
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0){
        SDL_Log("SDL can't be initialized: %s", SDL_GetError());
        return false;
    }

    /* Init SDL window */
    SDL_Window *window = SDL_CreateWindow(game->title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, game->fullscreen ? SDL_WINDOW_FULLSCREEN : 0);
    if(window == NULL){
        SDL_Log("SDL can't initialize window: %s", SDL_GetError());
        return false;
    }

	/* Init renderer */
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    if(renderer == NULL){
        SDL_Log("SDL can't initialize renderer: %s", SDL_GetError());
        return false;
    }

	/* Initial render clear */
    SDL_RenderClear(renderer);

	/* Assign parameters their new values */
    *gwindow = window;
    *grenderer = renderer;

    return true;
}

/**
 * @brief 
 * 
 * @param title 
 * @param w 
 * @param h 
 * @param fullscreen 
 * @return Game* 
 */
Game *StartGame(char *title, int w, int h, bool fullscreen){
	/* Allocate new game struct */
    Game *game = (Game *)malloc(sizeof(Game));
    if(game == NULL){
        SDL_Log("Can't allocate memory for the game.");
        return NULL;
    }

    /* Init game */
    game->title = title;
    WINDOW_WIDTH = w;
    WINDOW_HEIGHT = h;
    game->fullscreen = fullscreen;
    game->gameState = GAME_RUNNING;

	InitSDL(game, &game->window, &game->renderer);
	CreateEntityManager();
	CreateEventManager();
	CreateSpriteManager(game->renderer);
	CreateMapManager();
	CreatePhysicsManager();
	CreateGameLoader();
	CreateMTOLRenderer(game->renderer);
	LoadMap();
    return game;
}

/**
 * @brief 
 * 
 * @param game
 */
void GameLoop(Game *game){
	int previousTime = 0;
	double elapsedTime = 0;
	while(game->gameState != GAME_STOPPED){
		previousTime = SDL_GetTicks();
		SDL_Event event;
        while(SDL_PollEvent(&event)){
            switch (event.type)
            {
                case SDL_KEYDOWN:
					HandleInputEvent(&event);
					break;
                case SDL_KEYUP:
					HandleInputEvent(&event);
                    break;

                case SDL_QUIT:
                    game->gameState = GAME_STOPPED;
                    break;

                default:
                    break;
            }
        }
		InitEntities();
		PrepareToDrawEntities();
		UpdateEntities();
		UpdateTimeElapsed(elapsedTime);
		CallbackWithEntities(PhysicsUpdateEntity);
		CallbackWithEntitiesWithOtherEntities(CollisionCheckEntity);
		LateUpdateEntities();
		RenderClear();
		CallbackWithEntities(Draw);
		RenderPresent();
		elapsedTime = SDL_GetTicks()/1000.0 - previousTime/1000.0;
    }
}

/**
 * @brief 
 * 
 * @param game 
 */
void EndGame(Game *game){
	/* Destroy the game manager */
    DestroyGameManager();
    DestroyEventManager();
    DestroySpriteManager();
    DestroyMapManager();
    DestroyPhysicsManager();
    DestroyGameLoader();
    /* Free the allocated game */
    free(game);
}