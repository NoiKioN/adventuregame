/**
 * @file player.c
 * @author NoiKioN (nnightknight@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2018-11-05
 * 
 * @copyright Copyright (c) 2018
 * 
 */

#include "player.h"

static void HoldSprite(SingleSprite *sprite, int time){
	deltaTime = SDL_GetTicks() + time;
	heldSprite = sprite;
}

static void LoadSpriteTexture(SDL_Texture **spriteTexture, SpriteDefs *spriteDefs){
	*spriteTexture = GetSpriteTexture(spriteDefs->name);
	if (*spriteTexture == NULL){
		*spriteTexture = AddSpriteTexture(spriteDefs->name, spriteDefs->texturePath);
	}
}

static void LoadSprites(Entity *entity){
	SDL_Texture *spriteTexture;

	SpriteDefs *spriteDefs = GetDefs("cry_walk");
	LoadSpriteTexture(&spriteTexture, spriteDefs);
	SingleSprite *walk = CreateSingleSprite(spriteDefs->width, spriteDefs->height, spriteTexture, spriteDefs->frameTime, spriteDefs->numberOfFrames, spriteDefs->clips, spriteDefs->collider);

	spriteDefs = GetDefs("cry_idle");
	LoadSpriteTexture(&spriteTexture, spriteDefs);
	SingleSprite *idle = CreateSingleSprite(spriteDefs->width, spriteDefs->height, spriteTexture, spriteDefs->frameTime, spriteDefs->numberOfFrames, spriteDefs->clips, spriteDefs->collider);

	spriteDefs = GetDefs("cry_land");
	LoadSpriteTexture(&spriteTexture, spriteDefs);
	SingleSprite *land = CreateSingleSprite(spriteDefs->width, spriteDefs->height, spriteTexture, spriteDefs->frameTime, spriteDefs->numberOfFrames, spriteDefs->clips, spriteDefs->collider);

	spriteDefs = GetDefs("cry_jump");
	LoadSpriteTexture(&spriteTexture, spriteDefs);
	SingleSprite *jump = CreateSingleSprite(spriteDefs->width, spriteDefs->height, spriteTexture, spriteDefs->frameTime, spriteDefs->numberOfFrames, spriteDefs->clips, spriteDefs->collider);

	spriteDefs = GetDefs("cry_in_air");
	LoadSpriteTexture(&spriteTexture, spriteDefs);
	SingleSprite *inAir = CreateSingleSprite(spriteDefs->width, spriteDefs->height, spriteTexture, spriteDefs->frameTime, spriteDefs->numberOfFrames, spriteDefs->clips, spriteDefs->collider);

	spriteDefs = GetDefs("cry_crouch");
	LoadSpriteTexture(&spriteTexture, spriteDefs);
	SingleSprite *crouch = CreateSingleSprite(spriteDefs->width, spriteDefs->height, spriteTexture, spriteDefs->frameTime, spriteDefs->numberOfFrames, spriteDefs->clips, spriteDefs->collider);

	entity->sprites = (SingleSprite **)malloc(sizeof(SingleSprite *) * 6);
	entity->sprites[0] = idle;
	entity->sprites[1] = walk;
	entity->sprites[2] = jump;
	entity->sprites[3] = inAir;
	entity->sprites[4] = land;
	entity->sprites[5] = crouch;
}

void InitPlayer(Entity *entity) {
	Player *player = (Player *) malloc(sizeof(Player));
	if (player == NULL){
	    SDL_Log("Player can't be allocated.");
	    return;
	}

	double speed = 20.0, jumpVelocity = 200.0;

	entity->movingEntity = CreateMovingEnitity(jumpVelocity, speed, true, false, (SDL_Rect){.x = 0, .y = 0, .w = 0, .h = 0});
	SetMovingEntityFunctions(entity, &PlayerCollision);
    player->inputVector = CreateVectorDouble(0.0, 0.0);
    player->isCrouching = false;
    player->jumped = false;
    player->jumpedAnim = false;
    player->landed = false;
    player->isRevving = false;

    LoadSprites(entity);

    entity->currentSprite = entity->sprites[0];

    RegisterToInput(entity, A, PRESSED, &PlayerHandleInput);
    RegisterToInput(entity, W, PRESSED, &PlayerHandleInput);
	RegisterToInput(entity, S, PRESSED, &PlayerHandleInput);
	RegisterToInput(entity, D, PRESSED, &PlayerHandleInput);
	RegisterToInput(entity, SPACE, PRESSED, &PlayerHandleInput);
	RegisterToInput(entity, A, RELEASED, &PlayerHandleInput);
	RegisterToInput(entity, W, RELEASED, &PlayerHandleInput);
	RegisterToInput(entity, S, RELEASED, &PlayerHandleInput);
	RegisterToInput(entity, D, RELEASED, &PlayerHandleInput);
	RegisterToInput(entity, SPACE, RELEASED, &PlayerHandleInput);

    entity->data = player;
}

void UpdatePlayer(Entity *entity){
	Player *player = (Player *)entity->data;

	double velocityX = player->inputVector.x * entity->movingEntity.speed;
	double velocityY = entity->movingEntity.velocityVector.y;
	if (player->jumped && !entity->movingEntity.isInAir){
		player->jumped = false;
		entity->movingEntity.isInAir = true;
		velocityY = entity->movingEntity.jumpVelocity;
	}
	SetVelocity(&entity->movingEntity, CreateVectorDouble(velocityX, velocityY));

	entity->data = player;
}

void PlayerCollision(Entity *entity, Entity *collidedEntity, CollisionSide collisionSide){
	Player *player = (Player *)entity->data;
	if (collidedEntity != NULL) {
		if (strcmp(collidedEntity->type, "collider") == 0) {
			if (collisionSide == COLLISION_ABOVE) {
				entity->movingEntity.isInAir = false;
			}
		}
	}
}

void PrepareToDrawPlayer(Entity *entity){
	Player *player = (Player *)entity->data;
	entity->currentDirection = entity->movingEntity.velocityVector.x > 0 ? 1 : entity->movingEntity.velocityVector.x == 0 ? entity->currentDirection : -1;
	if (heldSprite != NULL){
		entity->currentSprite = heldSprite;
		if (SDL_GetTicks() > deltaTime){
			heldSprite = NULL;
		}
	}
	else {
		if (!entity->movingEntity.isInAir) {
			if (!(player->jumped || player->landed)) {
				if (entity->movingEntity.velocityVector.x == 0.0) {
					if (player->isCrouching) {
						entity->currentSprite = entity->sprites[5];
					}
					else if (player->isRevving){
						entity->currentSprite = entity->sprites[3];
					}
					else {
						entity->currentSprite = entity->sprites[0];
					}
				} else {
					entity->currentSprite = entity->sprites[1];
				}
			}
			if (player->jumpedAnim) {
				player->jumpedAnim = false;
				HoldSprite(entity->currentSprite = entity->sprites[2], 100);
			}
			if (player->landed) {
				player->landed = false;
				HoldSprite(entity->currentSprite = entity->sprites[4], 100);
			}
		}
		else {
			entity->currentSprite = entity->sprites[3];
		}
	}
	PrepareToDrawSingleSprite(entity->currentSprite, 0.7);
	SetVectorInt(&entity->size, entity->currentSprite->width * entity->scale, entity->currentSprite->height * entity->scale);
	entity->movingEntity.collider = entity->currentSprite->collider;
}

void DestroyPlayer(Entity *entity){
	Player *player = (Player *)entity->data;
	free(player);
	for (int i = 0; i < 6; ++i) {
		DestroySingleSprite(entity->sprites[i]);
	}
}

void RegisterPlayer(){
	Entity *entity = CreateEntity("player", "player", 2, 3, CreateVectorDouble(0.0, 0.0));
	SetEntityFunctions(entity, &InitPlayer, &UpdatePlayer, NULL, &PrepareToDrawPlayer, &DestroyPlayer);

	Enqueue(entity);
}

void PlayerHandleInput(Entity *entity, InputEvent *inputEvent){
	Player *player = (Player *)entity->data;
	if (inputEvent->inputPressType == PRESSED){
		switch (inputEvent->inputKeyEvent){
			case A:
				SetVectorDouble(&player->inputVector, -1.0, 0.0);
				break;
			case W:
				player->isRevving = true;
				break;
			case S:
				player->isCrouching = true;
				break;
			case D:
				SetVectorDouble(&player->inputVector, 1.0, 0.0);
				break;
			case SPACE:
				if (!entity->movingEntity.isInAir){
					player->jumped = true;
					player->jumpedAnim = true;
				}
				break;
			default:
				break;
		}
	}
	else if(inputEvent->inputPressType == RELEASED) {
		switch (inputEvent->inputKeyEvent){
			case S:
				player->isCrouching = false;
				break;
			case W:
				player->isRevving = false;
				break;
			case A:
			case D:
				SetVectorDouble(&player->inputVector, 0.0, player->inputVector.y);
				break;
			case SPACE:
				player->landed = true;
				break;
			default:
				break;
		}
	}
}