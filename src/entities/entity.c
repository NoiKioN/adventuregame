//
// Created by NoiKioN on 11/11/2018.
//

#include "entity.h"

Entity *CreateEntity(char *name, char *type, int layer, int scale, VectorDouble precisePosition){
	Entity *entity = (Entity *)malloc(sizeof(Entity));

	entity->name = name;
	entity->type = type;
	entity->scale = scale;
	entity->layer = layer;
	entity->currentDirection = 1;
	entity->precisePosition = precisePosition;
	entity->size = CreateVectorInt(0, 0);
	entity->movingEntity = CreateEmptyStaticMovingEntity();
	entity->data = NULL;
	entity->sprites = NULL;
	entity->currentSprite = NULL;

	entity->Init = NULL;
	entity->Update = NULL;
	entity->LateUpdate = NULL;
	entity->PrepareToDraw = NULL;
	entity->Destroy = NULL;

	return entity;
}

void DestroyEntity(Entity *entity){
	free(entity->sprites);
	free(entity);
}

void SetEntityFunctions(Entity *entity, void (*Init)(Entity *entity), void (*Update)(Entity *entity), void (*LateUpdate)(Entity *entity), void (*PrepareToDraw)(Entity *entity), void (*Destroy)(Entity *entity)){
	entity->Init = Init;
	entity->Update = Update;
	entity->LateUpdate = LateUpdate;
	entity->PrepareToDraw = PrepareToDraw;
	entity->Destroy = Destroy;
}

void SetMovingEntityFunctions(Entity *entity, void (*CollisionCallback)(Entity *entity, Entity *collidedEntity, CollisionSide collisionSide)){
	entity->CollisionCallback = CollisionCallback;
}