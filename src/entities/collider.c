//
// Created by nnigh on 12/5/2018.
//

#include "collider.h"

void RegisterCollider(SDL_Rect collider){
	Entity *colliderEntity = CreateEntity("collider", "collider", 1, 3, CreateVectorDouble(0.0, 0.0));
	SetEntityFunctions(colliderEntity, NULL, NULL, NULL, NULL, NULL);

	colliderEntity->movingEntity = CreateEmptyMovingEntityWithCollider(collider, false, true);
	Enqueue(colliderEntity);
}