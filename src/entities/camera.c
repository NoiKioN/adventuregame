//
// Created by nnigh on 11/19/2018.
//

#include "camera.h"

static double clampCamera(double pos, double min, double max){
	double temp = pos < min ? min : pos;
	return temp > max ? max : temp;
}

void RepositionToCameraView(Entity *entity, VectorInt *inputVector){
	inputVector->x = inputVector->x * entity->scale - (int)entity->precisePosition.x * entity->scale + WINDOW_WIDTH / 2;
	inputVector->y = inputVector->y * entity->scale - (int)entity->precisePosition.y * entity->scale + WINDOW_HEIGHT / 2;
}

void InitCamera(Entity *entity){
	Entity *playerEntity = GetEntityByName("player");

	entity->movingEntity = CreateEmptyStaticMovingEntity();
	entity->data = playerEntity;
	entity->currentSprite = NULL;
}

void LateUpdateCamera(Entity *entity){
	Entity *playerEntity = (Entity *)entity->data;
	double minX = (WINDOW_WIDTH / 2.0) / entity->scale;
	double maxX = MAP_WIDTH - minX;
	double minY = (WINDOW_HEIGHT / 2.0) / entity->scale;
	double maxY = MAP_HEIGHT - minY;

	double playerPosX = playerEntity->precisePosition.x + (playerEntity->size.x / 4.0);
	double playerPosY = playerEntity->precisePosition.y + (playerEntity->size.y / 4.0);

	SetVectorDouble(&entity->precisePosition, clampCamera(playerPosX, minX, maxX), clampCamera(playerPosY, minY, maxY));
}

void RegisterCamera(){
	Entity *entity = CreateEntity("camera", "camera", 3, 3, CreateVectorDouble(0.0, 0.0));
	SetEntityFunctions(entity, &InitCamera, NULL, &LateUpdateCamera, NULL, NULL);

	Enqueue(entity);
}