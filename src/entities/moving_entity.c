//
// Created by NoiKioN on 11/15/2018.
//

#include "moving_entity.h"

MovingEntity CreateMovingEnitity(double jumpVelocity, double speed, bool isInAir, bool isStatic, SDL_Rect collider){
	MovingEntity movingEntity = (MovingEntity){
		.velocityVector = CreateVectorDouble(0.0, 0.0),
		.jumpVelocity = jumpVelocity,
		.speed = speed,
		.isInAir = isInAir,
		.isStatic = isStatic,
		.collider = collider,
	};

	return movingEntity;
}

MovingEntity CreateEmptyMovingEntityWithCollider(SDL_Rect collider, bool isInAir, bool isStatic){
	MovingEntity movingEntity = (MovingEntity){
			.velocityVector = CreateVectorDouble(0.0, 0.0),
			.jumpVelocity = 0,
			.speed = 0,
			.isInAir = isInAir,
			.isStatic = isStatic,
			.collider = collider,
	};

	return movingEntity;
}

MovingEntity CreateEmptyStaticMovingEntity(){
	MovingEntity movingEntity = (MovingEntity){
		.velocityVector = CreateVectorDouble(0.0, 0.0),
		.jumpVelocity = 0,
		.speed = 0,
		.isInAir = false,
		.isStatic = true,
		.collider = (SDL_Rect){
			.x = 0,
			.y = 0,
			.w = 0,
			.h = 0
		},
	};

	return movingEntity;
}

void SetVelocity(MovingEntity *movingEntity, VectorDouble velocity){
	movingEntity->velocityVector = velocity;
}

SDL_Rect GetMovingEntityCollider(MovingEntity movingEntity, VectorInt position){
	SDL_Rect colliderWithPos = (SDL_Rect){
		.x = movingEntity.collider.x + position.x,
		.y = movingEntity.collider.y + position.y,
		.w = movingEntity.collider.w,
		.h = movingEntity.collider.h,
	};

	return colliderWithPos;
}