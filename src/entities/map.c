//
// Created by NoiKioN on 11/25/2018.
//

#include "map.h"

void DestroyMap(Entity *entity){
	free(entity->data);
	DestroySingleSprite(entity->currentSprite);
}

void RegisterMap(char *name, char *type, int layer, SDL_Texture *mapTexture, SDL_Rect clip){
	Entity *entity = CreateEntity(name, type, layer, 3, CreateVectorDouble(0.0, 0.0));
	SetEntityFunctions(entity, NULL, NULL, NULL, NULL, &DestroyMap);

	SingleSprite *singleSprite = CreateSingleSprite(clip.w, clip.h, mapTexture, 0, 1, &clip, clip);
	entity->currentSprite = singleSprite;
	entity->movingEntity = CreateEmptyStaticMovingEntity();

	Enqueue(entity);
}