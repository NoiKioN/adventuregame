/**
 * @file mtol_def.c
 * @author NoiKioN (nnightknight@gmail.com)
 * @brief Reading the .mtol files describing different aspects of the game.
 * @version 1.0
 * @date 2018-11-02
 * 
 * @copyright Copyright (c) 2018
 */

#include "test/debugmalloc.h"


#include "mtol_def.h"
#include <stdio.h>
#include "SDL2/SDL.h"
#include <string.h>
#include <ctype.h>

/**
 * @brief Reading the definition file, providing the char array, and the length of the array.
 * 
 * @param path      The path to the definition file.
 * @param read_file The read file in a char array.
 * @param length    The length of the file_read array
 */
static void ReadDefFile(const char *path, char **read_file, int *length){
    FILE *file = fopen(path, "rb");
    if(file == NULL){
        SDL_Log("File could not be read: %s", path);
        return;
    }

    fseek(file, 0, SEEK_END);

    int resourceSize = ftell(file);
    rewind(file);

    char *resource = (char *) malloc(sizeof(char) * ((unsigned)resourceSize + 1));
    if(resource == NULL){
        SDL_Log("Read file could not be allocated.");
        return;
    }

    fread(resource, (unsigned)resourceSize, 1, file);
    fclose(file);
    
    resource[resourceSize] = '\0';

    int resourceWithoutNewLineLength = 0;
	for (int i = 0; i < resourceSize; ++i) {
		while(resource[i] == '\n' || resource[i] == '\r') ++i;
		++resourceWithoutNewLineLength;
	}

    char *resourceWithoutNewLine = (char *)malloc(sizeof(char) * (resourceWithoutNewLineLength + 1));

	resourceWithoutNewLineLength = 0;
	for (int i = 0; i < resourceSize; ++i) {
		while(resource[i] == '\n' || resource[i] == '\r') ++i;
		resourceWithoutNewLine[resourceWithoutNewLineLength] = resource[i];
		++resourceWithoutNewLineLength;
	}

	free(resource);
	resourceWithoutNewLine[resourceWithoutNewLineLength] = '\0';

    *read_file = resourceWithoutNewLine;
    *length = resourceWithoutNewLineLength;
}

/**
 * @brief Counts the number of key-value pairs in the definition file.
 * 
 * @param read_file     The read file char array.
 * @param length        The length of the read_file array.
 * @param num_of_kvs    The pointer of the number of key-value pairs.
 */
static void CountMTOLDefinitions(const char *read_file, int length, int *num_of_kvs){
    for(int i = 0; i < length; ++i){
        if(read_file[i] == ':'){
            ++(*num_of_kvs);
        }
    }
}

static char *TrimSpace(char *str) {
    char *end;

    // Trim leading space
    while(isspace((unsigned char)*str)) str++;

    if(*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;

    // Write new null terminator character
    end[1] = '\0';

    return str;
}


/**
 * @brief Get the Next Key-Value pair.
 * 
 * @param readFile         The read file char array.
 * @param key               The key read.
 * @param value             The value read.
 * @param already_read_to   The index already read to.
 */
static void GetNextKeyValue(const char *readFile, char **p_key, char **p_value, int *already_read_to){
    int key_begin = *already_read_to, key_end = 0;
    int value_begin = 0, value_end = 0;
    int i = *already_read_to;
    int keyWOSpace = 0;
    while(true){
        if(readFile[i] != ' '){
            if(key_end == 0) {
                ++keyWOSpace;
            }
        }
        if (readFile[i] == ':') {
            value_begin = i + 1;
            key_end = i;
        }
        if (readFile[i] == ';') {
            value_end = i;
            break;
        }
        ++i;
    }

    *already_read_to = i + 1;

	int valueLength = value_end - value_begin;
    char *key = (char *)malloc((keyWOSpace) * sizeof(char));
    char *value = (char *)malloc((valueLength + 1) * sizeof(char));

    int k = 0;
    for(int j = key_begin; j < key_end; j++)
    {
        if(readFile[j] != ' ') {
            key[k] = readFile[j];
            ++k;
        }
    }

    k = 0;
    for(int j = value_begin; j < value_end; j++) {
		value[k] = readFile[j];
		++k;
    }

    key[keyWOSpace - 1] = '\0';
	value[valueLength] = '\0';

	char *valueWOExcessSpace = (char *)malloc((strlen(TrimSpace(value)) + 1) * sizeof(char));
	strcpy(valueWOExcessSpace, TrimSpace(value));

	free(value);

    *p_key = key;
    *p_value = valueWOExcessSpace;
}

/**
 * @brief Free the space in memory used by the specified definitions in definitions.
 * 
 * @param definitions A pointer to a definitions struct in memory.
 */
void FreeMTOLDefs(MTOL_Definitions *definitions){
    for(int i = 0; i < definitions->numberOfDefs; ++i)
    {
        free(definitions->keys[i]);
        definitions->keys[i] = NULL;
        free(definitions->values[i]);
        definitions->values[i] = NULL;
    }
    free(definitions->keys);
    definitions->keys = NULL;
    free(definitions->values);
    definitions->values = NULL;
    free(definitions);
}

/**
 * @brief Get the MTOL Definitions from a file.
 * 
 * @param path                  The path to the definitions file.
 * @return MTOL_Definitions*    The definitions struct.
 */
MTOL_Definitions *GetDefinitions(const char *path){
    MTOL_Definitions *definitions = (MTOL_Definitions *)malloc(sizeof(MTOL_Definitions));
    if(definitions == NULL){
        SDL_Log("Definitions could not be allocated.");
        return NULL;
    }
    char *read_file = NULL;
    int length = 0;
    ReadDefFile(path, &read_file, &length);
    
    if(read_file == NULL){
        SDL_Log("File couldn't be read: %s", path);
        free(definitions);
        return NULL;
    }

    int numOfKvs = 0;

    CountMTOLDefinitions(read_file, length, &numOfKvs);
    if(numOfKvs == 0){
        SDL_Log("There are no key-value pairs.");
    }
    
    definitions->numberOfDefs = numOfKvs;
    definitions->keys = (char **)malloc(numOfKvs * sizeof(char *));
    if(definitions->keys == NULL){
        SDL_Log("Keys could not be allocated.");
        FreeMTOLDefs(definitions);
        return NULL;
    }
    definitions->values = (char **)malloc(numOfKvs * sizeof(char *));
    if(definitions->values == NULL){
        SDL_Log("Values could not be allocated.");
        FreeMTOLDefs(definitions);
        return NULL;
    }
    int already_read_to = 0;

    for(int y = 0; y < numOfKvs; ++y){
        definitions->keys[y] = NULL;
        definitions->values[y] = NULL;

        GetNextKeyValue(read_file, &definitions->keys[y], &definitions->values[y], &already_read_to);
        if(definitions->keys[y] == NULL){
            SDL_Log("Key could not be allocated.");
            FreeMTOLDefs(definitions);
            return NULL;
        }
        if(definitions->values[y] == NULL){
            SDL_Log("Value could not be allocated.");
            FreeMTOLDefs(definitions);
            return NULL;
        }
    }

    free(read_file);
    read_file = NULL;

    return definitions;
}