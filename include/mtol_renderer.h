//
// Created by NoiKioN on 11/17/2018.
//

#ifndef MTOLEGENDS_MTOL_RENDERER_H
#define MTOLEGENDS_MTOL_RENDERER_H

#include "SDL2/SDL_render.h"
#include "sprite.h"
#include "entity.h"
#include "entity_manager.h"
#include "camera.h"

SDL_Renderer *MTOL_RENDERER;

void CreateMTOLRenderer(SDL_Renderer *renderer);

void Draw(Entity *entity);

void RenderClear();

void RenderPresent();

#endif //MTOLEGENDS_MTOL_RENDERER_H
