//
// Created by nnigh on 11/15/2018.
//

#ifndef MTOLEGENDS_EVENT_H
#define MTOLEGENDS_EVENT_H

#include "test/debugmalloc.h"

#include "SDL2/SDL.h"

typedef enum InputKeyEvent{
	EMPTY_KEY_EVENT, W = SDLK_w, A = SDLK_a, S = SDLK_s, D = SDLK_d, UP = SDLK_UP, LEFT = SDLK_LEFT, DOWN = SDLK_DOWN, RIGHT = SDLK_RIGHT, SPACE = SDLK_SPACE, LAST_KEY_EVENT
} InputKeyEvent;

typedef enum InputPressType{
	EMPTY_PRESS_TYPE, PRESSED, RELEASED
} InputPressType;

typedef struct InputEvent{
	InputKeyEvent inputKeyEvent;
	InputPressType inputPressType;
} InputEvent;

#endif //MTOLEGENDS_EVENT_H
