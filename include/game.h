#ifndef GAME_HEADER
#define GAME_HEADER

#include "test/debugmalloc.h"

#include "SDL2/SDL.h"
#include "entity_manager.h"
#include "sprite_manager.h"
#include "event_manager.h"
#include "map_manager.h"
#include "physics_manager.h"
#include "game_loader.h"
#include "mtol_renderer.h"
#include "shared_properties.h"

typedef enum GameState {
    GAME_RUNNING, GAME_PAUSED, GAME_STOPPED
} GameState;

typedef struct Game {
    char *title;
    bool fullscreen;
    GameState gameState;
	SDL_Renderer *renderer;
	SDL_Window *window;
} Game;

Game *StartGame(char *title, int w, int h, bool fullscreen);

void GameLoop(Game *game);

void EndGame(Game *game);

#endif