//
// Created by NoiKioN on 11/8/2018.
//

#ifndef MTOLEGENDS_EVENT_MANAGER_H
#define MTOLEGENDS_EVENT_MANAGER_H

#include <stdbool.h>
#include <stdlib.h>
#include "include/entities/entity.h"
#include "event.h"

typedef struct InputRegistryListElement{
	Entity *entity;
	InputEvent *inputListened;
	void (*Callback)(Entity *entity, InputEvent *inputEvent);
	struct InputRegistryListElement *next;
} InputRegistryListElement;

typedef struct EventManager{
	bool acceptInput;
	InputRegistryListElement *first;
	InputRegistryListElement *last;
	InputEvent *currentInputEvent;
} EventManager;

static EventManager *EVENT_MANAGER;

EventManager *CreateEventManager();

void RegisterToInput(Entity *entity, InputKeyEvent keyEvent, InputPressType inputPressType, void (*Callback)(Entity *entity, InputEvent *inputEvent));

void HandleInputEvent(SDL_Event *event);

void CallbackToEntity(InputEvent *inputEvent);

void DestroyEventManager();

#endif //MTOLEGENDS_EVENT_MANAGER_H
