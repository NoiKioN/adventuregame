//
// Created by nnigh on 11/19/2018.
//

#ifndef MTOLEGENDS_MAP_MANAGER_H
#define MTOLEGENDS_MAP_MANAGER_H

#include <stdio.h>

#include "SDL2/SDL_image.h"

#include "map.h"
#include "player.h"
#include "camera.h"
#include "mtol_renderer.h"
#include "entity_manager.h"
#include "entities/collider.h"

typedef struct MapManager{
	Entity *cameraEntity;
} MapManager;

static MapManager *MAP_MANAGER;

MapManager *CreateMapManager();

void LoadMap();

Entity *GetMainCamera();

void DestroyMapManager();

SDL_Rect getCurrentColliderRect();

#endif //MTOLEGENDS_MAP_MANAGER_H
