//
// Created by nnigh on 11/21/2018.
//

#ifndef MTOLEGENDS_PHYSICS_MANAGER_H
#define MTOLEGENDS_PHYSICS_MANAGER_H

#include <stdlib.h>
#include "entity_manager.h"
#include "map_manager.h"

typedef struct PhysicsManager{
	double gravityVelocity;

	double deltaTime;
} PhysicsManager;

static PhysicsManager *PHYSICS_MANAGER;

PhysicsManager *CreatePhysicsManager();

void CollisionCheckEntity(Entity *entity, Entity *collidedEntity);

void UpdateTimeElapsed(double timeInMS);

void PhysicsUpdateEntity(Entity *entity);

void DestroyPhysicsManager();

#endif //MTOLEGENDS_PHYSICS_MANAGER_H
