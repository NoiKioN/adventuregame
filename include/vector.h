//
// Created by nnigh on 11/14/2018.
//

#ifndef MTOLEGENDS_VECTOR_H
#define MTOLEGENDS_VECTOR_H

#include "test/debugmalloc.h"

#include <stdlib.h>
#include <math.h>

typedef struct VectorDouble{
    double x, y;
} VectorDouble;

typedef struct VectorInt{
	int x, y;
} VectorInt;

/* VECTOR DOUBLE */

VectorDouble CreateVectorDouble(double x, double y);

void SetVectorDouble(VectorDouble *pVectorDouble, double x, double y);

VectorDouble ConvertVectorIntToVectorDouble(VectorInt vectorInt);

VectorInt GetVectorIntFromVectorDouble(VectorDouble vectorDouble);

void CopyVectorDouble(VectorDouble *pVectorDouble, VectorDouble vectorDouble);

void MultiplyVectorDouble(VectorDouble *pVectorDouble, VectorDouble vectorDouble);

VectorDouble MultiplyVectorDoubleWithC(VectorDouble vectorDouble, double c);

void AddVectorDouble(VectorDouble *pVectorDouble, VectorDouble vectorDouble);

/* VECTOR INT */

VectorInt CreateVectorInt(int x, int y);

void CopyVectorInt(VectorInt *pVectorInt, VectorInt vectorInt);

VectorInt ConvertVectorDoubleToVectorInt(VectorDouble vectorDouble);

void SetVectorInt(VectorInt *vector, int x, int y);

void AddVectorInt(VectorInt *vector, int x, int y);

#endif //MTOLEGENDS_VECTOR_H
