#ifndef mtol_def
#define mtol_def

#include "test/debugmalloc.h"

typedef struct MTOL_Definitions{
    char **keys;
    char **values;
    int numberOfDefs;
} MTOL_Definitions;

MTOL_Definitions *GetDefinitions(const char *path);
void FreeMTOLDefs(MTOL_Definitions *definitions);

#endif