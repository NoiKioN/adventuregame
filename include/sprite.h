//
// Created by NoiKioN on 11/8/2018.
//

#ifndef MTOLEGENDS_SPRITE_H
#define MTOLEGENDS_SPRITE_H

#include "test/debugmalloc.h"

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "mtol_def.h"
#include "vector.h"
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

typedef struct ClipRectListElement {
	SDL_Rect clipRect;
	struct ClipRectListElement *nextClipRectListElement;
} ClipRectListElement;

typedef struct SingleSprite {
	SDL_Texture *spriteTexture;
	ClipRectListElement *firstClipRectListElement;
	int width, height;
	int numberOfFrames, frameTime, currentFrameTime;
	ClipRectListElement *currentClipRectListElement;
	SDL_Rect collider;
} SingleSprite;

SingleSprite *CreateSingleSprite(int width, int height, SDL_Texture *spriteTexture, int frameTime, int numberOfFrames, SDL_Rect *clips, SDL_Rect collider);

void PrepareToDrawSingleSprite(SingleSprite *singleSprite, double drawAcceleration);

void DestroySingleSprite(SingleSprite *sprite);

#endif //MTOLEGENDS_SPRITE_H