//
// Created by NoiKioN on 11/11/2018.
//

#ifndef MTOLEGENDS_ENTITY_H
#define MTOLEGENDS_ENTITY_H

#include "test/debugmalloc.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "sprite.h"
#include "vector.h"
#include "moving_entity.h"

typedef struct Entity {
	char *name;
	char *type;
	void *data;
	int scale;
	int layer;
	VectorDouble precisePosition;
	MovingEntity movingEntity;
	VectorInt size;
	SingleSprite **sprites;
	SingleSprite *currentSprite;
	int currentDirection;

	void (*Init)(struct Entity *entity);
	void (*Update)(struct Entity *entity);
	void (*LateUpdate)(struct Entity *entity);
	void (*PrepareToDraw)(struct Entity *entity);
	void (*Destroy)(struct Entity *entity);

	void (*CollisionCallback)(struct Entity *entity, struct Entity *collidedEntity, CollisionSide collisionSide);
} Entity;

Entity *CreateEntity(char *name, char *type, int layer, int scale, VectorDouble precisePosition);

void SetEntityFunctions(Entity *entity, void (*Init)(Entity *entity), void (*Update)(Entity *entity), void (*LateUpdate)(Entity *entity), void (*PrepareToDraw)(Entity *entity), void (*Destroy)(Entity *entity));

void SetMovingEntityFunctions(Entity *entity, void (*CollisionCallback)(Entity *entity, Entity *collidedEntity, CollisionSide collisionSide));

void DestroyEntity(Entity *entity);

#endif //MTOLEGENDS_ENTITY_H
