//
// Created by nnigh on 12/5/2018.
//

#ifndef MTOLEGENDS_COLLIDER_H
#define MTOLEGENDS_COLLIDER_H

#include "SDL2/SDL_rect.h"
#include "entity.h"
#include "entity_manager.h"

void RegisterCollider(SDL_Rect collider);

#endif //MTOLEGENDS_COLLIDER_H
