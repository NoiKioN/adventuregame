//
// Created by NoiKioN on 11/25/2018.
//

#ifndef MTOLEGENDS_TILE_H
#define MTOLEGENDS_TILE_H

#include "sprite_manager.h"
#include "game_loader.h"
#include "entity_manager.h"
#include "entity.h"

void DestroyMap(Entity *entity);

void RegisterMap(char *name, char *type, int layer, SDL_Texture *mapTexture, SDL_Rect clip);

#endif //MTOLEGENDS_TILE_H
