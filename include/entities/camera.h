//
// Created by nnigh on 11/19/2018.
//

#ifndef MTOLEGENDS_CAMERA_H
#define MTOLEGENDS_CAMERA_H

#include "moving_entity.h"
#include "entity.h"
#include "player.h"
#include "shared_properties.h"
#include "entity_manager.h"

void RepositionToCameraView(Entity *entity, VectorInt *inputVector);

void InitCamera(Entity *entity);

void LateUpdateCamera(Entity *entity);

void DestroyCamera(Entity *entity);

void RegisterCamera();

#endif //MTOLEGENDS_CAMERA_H
