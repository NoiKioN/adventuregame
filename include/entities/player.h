#ifndef player_header
#define player_header

#include "test/debugmalloc.h"

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "sprite.h"
#include "entity.h"
#include "event.h"
#include "moving_entity.h"
#include "event_manager.h"
#include "sprite_manager.h"
#include "entity_manager.h"
#include "game_loader.h"

/* The player struct containing all specs of a player needed */
typedef struct Player{
	VectorDouble inputVector;
	bool isCrouching;
	bool jumped;
	bool jumpedAnim;
	bool landed;
	bool isRevving;
} Player;

static int deltaTime;
static SingleSprite *heldSprite;

void InitPlayer(Entity *entity);

void UpdatePlayer(Entity *entity);

void PlayerCollision(Entity *entity, Entity *collidedEntity, CollisionSide collisionSide);

void PrepareToDrawPlayer(Entity *entity);

void DestroyPlayer(Entity *entity);

void RegisterPlayer();

void PlayerHandleInput(Entity *entity, InputEvent *inputEvent);

#endif