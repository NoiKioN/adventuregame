//
// Created by NoiKioN on 11/15/2018.
//

#ifndef MTOLEGENDS_MOVING_ENTITY_H
#define MTOLEGENDS_MOVING_ENTITY_H

#include "test/debugmalloc.h"

#include <stdbool.h>
#include "SDL2/SDL_rect.h"
#include "vector.h"

typedef struct MovingEntity {
	VectorDouble velocityVector;
	double jumpVelocity;
	double speed;
	bool isInAir;
	bool isStatic;
	SDL_Rect collider;
} MovingEntity;

typedef enum CollisionSide{
	COLLISION_LEFT, COLLISION_BELOW, COLLISION_RIGHT, COLLISION_ABOVE
} CollisionSide;

MovingEntity CreateMovingEnitity(double jumpVelocity, double speed, bool isInAir, bool isStatic, SDL_Rect collider);

MovingEntity CreateEmptyMovingEntityWithCollider(SDL_Rect collider, bool isInAir, bool isStatic);

MovingEntity CreateEmptyStaticMovingEntity();

void SetVelocity(MovingEntity *movingEntity, VectorDouble velocity);

SDL_Rect GetMovingEntityCollider(MovingEntity movingEntity, VectorInt position);

#endif //MTOLEGENDS_MOVING_ENTITY_H
