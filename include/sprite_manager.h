//
// Created by nnigh on 11/9/2018.
//

#ifndef MTOLEGENDS_SPRITE_MANAGER_H
#define MTOLEGENDS_SPRITE_MANAGER_H

#include "test/debugmalloc.h"

#include <stdbool.h>
#include <stdlib.h>
#include "sprite.h"
#include "SDL2/SDL_image.h"

typedef struct SpriteTextureListElement{
	char *name;
	SDL_Texture *spriteTexture;
	struct SpriteTextureListElement *next;
} SpriteTextureListElement;

typedef struct SpriteManager{
	SDL_Renderer *renderer;
	SpriteTextureListElement *first;
	SpriteTextureListElement *last;
} SpriteManager;

static SpriteManager *SPRITE_MANAGER;

SpriteManager *CreateSpriteManager(SDL_Renderer *renderer);

SDL_Texture *GetSpriteTexture(char *name);

SDL_Texture *AddSpriteTexture(char *name, char *texturePath);

void RemoveSpriteTexture(char *name);

void DestroySpriteManager();

#endif //MTOLEGENDS_GAME_LOADER_H
