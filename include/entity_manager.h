#ifndef GAME_MANAGER_HEADER
#define GAME_MANAGER_HEADER

#include "test/debugmalloc.h"

#include "SDL2/SDL.h"
#include "entities/entity.h"

typedef struct EntityListElement {
	Entity *entity;
	struct EntityListElement *nextElement;
	struct EntityListElement *previousElement;
} EntityListElement;

typedef struct EntityManager {
	EntityListElement *first;
	EntityListElement *last;
} EntityManager;

static EntityManager *ENTITY_MANAGER;

EntityManager *CreateEntityManager();

void Enqueue(Entity *entity);

void Dequeue(Entity *entity);

void DestroyGameManager();

Entity *GetEntityByName(char *name);

void CallbackWithEntities(void Function(Entity *entity));

void CallbackWithEntitiesWithOtherEntities(void Function(Entity *firstEntity, Entity *secondEntity));

void InitEntities();

void UpdateEntities();

void LateUpdateEntities();

void PrepareToDrawEntities();

#endif