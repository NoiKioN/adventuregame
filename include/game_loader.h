//
// Created by NoiKioN on 11/18/2018.
//

#ifndef MTOLEGENDS_GAME_LOADER_H
#define MTOLEGENDS_GAME_LOADER_H

#include "SDL2/SDL.h"
#include "mtol_def.h"
#include "string.h"
#include "dirent.h"
#include "vector.h"
#include <ctype.h>

enum SpriteDefTypes{
	SPRITE_TEXTURE, SPRITE_FRAMES, SPRITE_FRAME_TIME, SPRITE_CLIP, SPRITE_COLLIDER, SPRITE_SIZE
};
static char *spriteDefTypeNames[7] = {"sprite", "frames", "frametime", "clip", "collider", "size"};

typedef struct SpriteDefs{
	char *name;
	char *texturePath;
	int numberOfFrames;
	int frameTime;
	int width, height;
	SDL_Rect *clips;
	SDL_Rect collider;
} SpriteDefs;

enum MapDefTypes{
	MAP_SIZE, TILE_SIZE, MAP_TILE, MAP_LAYER, COLLIDER
};
static char *mapDefTypeNames[5] = {"mapsize", "tilesize", "tile", "layer", "collider"};

typedef struct MapDefs{
	int numOfLayers;
	VectorInt mapSize;
	VectorInt tileSize;
	int ***layers;
	int numOfTiles;
	char **mapTiles;
	SDL_Rect *colliders;
	int numberOfColliders;
} MapDefs;

enum DefinitionListElementType{
	SPRITE, MAP
};

typedef struct DefinitionListElement{
	char *name;
	enum DefinitionListElementType type;
	void *defs;
	struct DefinitionListElement *next;
} DefinitionListElement;

typedef struct GameLoader{
	DefinitionListElement *first;
	DefinitionListElement *last;
} GameLoader;

static GameLoader *GAME_LOADER;

GameLoader *CreateGameLoader();

void *GetDefs(char *name);

void DestroyGameLoader();

#endif //MTOLEGENDS_GAME_LOADER_H
